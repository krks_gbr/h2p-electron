


import $ from 'jquery';
import cssRegions from '../css-regions';
import masterPageHTML from './html/masterpage.html';


let makeLayout = function({html, postProcess, mirror}){

    // make pages wrapper
    let $pages = $('<div>').attr('id', 'pages');

    if(mirror){
        $pages.addClass('mirrored');
    }

    // make content source wrapper
        $('<div>')
            .attr('id', 'content-source')
            .html(html)
            .appendTo('body');


    // make masterpage template
    let $masterPageTemplate = $(
        $.parseHTML(masterPageHTML).find(el => $(el).hasClass('masterPage'))
    );


    // create pages
    for (let i = 0; i<50; i++){
        console.log('creating page ', i);
        let page = $masterPageTemplate
            .clone()
            .attr("id", "page-" + i).appendTo($pages);
        page.find('.h2p-body').addClass('content-target');
    }


    $pages.prependTo('body');



    // lock scroll
    $('html body').css({

        overflow: 'hidden',

    });




    return new Promise((resolve, reject) => {



        cssRegions.enablePolyfill();



        // wait until flow is finished
        let flow = document.getNamedFlow('contentflow');
        flow.addEventListener('regionfragmentchange', () => {

            console.log('flow done');




            // postProcess callback,
            if(postProcess){
                postProcess($pages[0], $);
            }





            // remove empty pages
            let empty =
                $pages
                    .find('.paper')
                    .filter( (i, paper)  => $(paper).find('cssregion').children().length === 0);

            empty.remove();






            // the postprocess script might have triggered a reflow process,
            // so we have to wait for it to finish

            let wait = setInterval(function(){
                let isFlowFinished = !flow.relayoutInProgress && !flow.relayoutScheduled;
                if(isFlowFinished){


                    //restore scrolling

                    $('html, body').css({

                        overflow: 'initial',

                    });

                    resolve();

                    clearInterval(wait);

                }
            }, 100);


        });
    });

};


export default makeLayout;