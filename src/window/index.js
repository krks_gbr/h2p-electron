

import makeLayout from './lib/h2p';
import $ from 'jquery';
import {ipcRenderer as ipc} from 'electron';








$(document).ready(() => {

    const remote = require('electron').remote;
    const html = remote.getGlobal('html');
    const h2pStyle = remote.getGlobal('h2pStyle');
    const userStyle = remote.getGlobal('userStyle');
    const pageConfig = remote.getGlobal('pageConfig');
    const postProcessAsString = remote.getGlobal('postProcess');


    const postProcess = new Function(`return ${postProcessAsString}`)();


    $('head').append(
        `
        <style>${h2pStyle}</style>
        <style>${userStyle}</style>
        `
    );

    makeLayout({html: html, postProcess: postProcess, mirror: pageConfig.mirror}).then( () => {
        ipc.send('done');
    });

});

