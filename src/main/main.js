


const {app, BrowserWindow, dialog} = require('electron');
const fs = require('fs');
const electron = require('electron');
const {ipcMain} = electron;
const path = require('path');
const stylus = require('stylus');


const {htmlFile, cssFile, pageConfigFile, outputFile, withGUI} = JSON.parse(process.argv[2]);





dialog.showErrorBox = function(title, content) {
    throw new Error(`${title}\n${content}`);
};

const compileStylus = (styleIndex, dimensions) => {

    const createStylusPageConfig = dimensions =>{
        let stylusPageConfig = {};
        Object.keys(dimensions).forEach(key => stylusPageConfig[key] = new stylus.nodes.Literal(dimensions[key]));
        return stylusPageConfig;
    };

    return new Promise((resolve, reject) => {
        stylus('')
            .define('page', createStylusPageConfig(dimensions), true)
            .set('filename', styleIndex)
            .import(styleIndex)
            .render((err, h2pCSS) => {

                if(err){
                    reject(err);
                }

                resolve(h2pCSS);

            });
    });
};



const getWindowOptions = () => {
    if(!withGUI){
        return {width: 0, height: 0, show: false};
    } else {
        const display = electron.screen.getAllDisplays()[0];
        const {width, height} = display.workArea;

        return {width: width, height: height, show: true}
    }
};



app.on('ready', () => {

    const indexHTML = path.resolve(`${__dirname}/../window/index.html` );
    const window = new BrowserWindow(getWindowOptions());


    window.toggleDevTools();


    window.loadURL( `file://${indexHTML}` );
    window.webContents.on('did-finish-load', () => {

        delete require.cache[require.resolve(pageConfigFile)];
        const config = require(pageConfigFile);

        const stylesDir = path.resolve(__dirname,'../style');

        compileStylus(`${stylesDir}/index.styl` , config.page.dimensions).then(h2pStyle => {
            global.html =  fs.readFileSync(htmlFile, 'utf8');
            global.h2pStyle = h2pStyle;
            global.userStyle = fs.readFileSync(cssFile, 'utf8');
            global.pageConfig = config.page;
            global.postProcess = config.postProcess.toString();
        });

    });


    const print = () =>
        window.webContents.printToPDF({}, (err, data) => {
            if (err) {
                throw err;
            }

            fs.writeFile(outputFile, data, err => {

                if (err) {
                    throw err;
                }

                if(!withGUI){
                    app.quit();
                }

            });

        });


    ipcMain.on('done', e => {
        console.log('main received done event from renderer');
        print();
    });

});








