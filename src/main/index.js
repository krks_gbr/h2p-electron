


import {spawn} from 'child_process';
import electron from 'electron';
import path from 'path';




export default function(options){



    const requiredOptionKeys = ['htmlFile', 'cssFile', 'pageConfigFile', 'outputFile'];
    const missingOptionKeys = requiredOptionKeys.filter(rKey =>  Object.keys(options).find(oKey => rKey === oKey) === undefined );

    if(missingOptionKeys.length > 0){
        throw new Error(`missing these required options: ${missingOptionKeys.join(', ')}`);
    }

    const electronScriptPath = path.resolve(`${__dirname}/main.js`);
    const electronProcess = spawn(electron, [ electronScriptPath, JSON.stringify(options) ]);

    return new Promise((resolve, reject) => {

        electronProcess.stdout.on('data', data => console.log(data.toString('utf8')));

        electronProcess.on('error', err => reject('failed to run electron process'));

        electronProcess.stderr.on('data', data => {

            const err = data.toString('utf8');

            //ignore this error
            if(err.indexOf("selectedTextBackgroundColor") !== -1){
                return
            }

            reject(data.toString('utf8'));
            electronProcess.kill();

        });

        electronProcess.on('exit', code => {
            if(code === 0){
                resolve();
            }
        });
    });

};








