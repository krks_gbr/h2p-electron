'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (options) {

    var requiredOptionKeys = ['htmlFile', 'cssFile', 'pageConfigFile', 'outputFile'];
    var missingOptionKeys = requiredOptionKeys.filter(function (rKey) {
        return Object.keys(options).find(function (oKey) {
            return rKey === oKey;
        }) === undefined;
    });

    if (missingOptionKeys.length > 0) {
        throw new Error('missing these required options: ' + missingOptionKeys.join(', '));
    }

    var electronScriptPath = _path2.default.resolve(__dirname + '/main.js');
    var electronProcess = (0, _child_process.spawn)(_electron2.default, [electronScriptPath, JSON.stringify(options)]);

    return new Promise(function (resolve, reject) {

        electronProcess.stdout.on('data', function (data) {
            return console.log(data.toString('utf8'));
        });

        electronProcess.on('error', function (err) {
            return reject('failed to run electron process');
        });

        electronProcess.stderr.on('data', function (data) {

            var err = data.toString('utf8');

            //ignore this error
            if (err.indexOf("selectedTextBackgroundColor") !== -1) {
                return;
            }

            reject(data.toString('utf8'));
            electronProcess.kill();
        });

        electronProcess.on('exit', function (code) {
            if (code === 0) {
                resolve();
            }
        });
    });
};

var _child_process = require('child_process');

var _electron = require('electron');

var _electron2 = _interopRequireDefault(_electron);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

;
module.exports = exports['default'];