'use strict';

var _require = require('electron');

var app = _require.app;
var BrowserWindow = _require.BrowserWindow;
var dialog = _require.dialog;

var fs = require('fs');
var electron = require('electron');
var ipcMain = electron.ipcMain;

var path = require('path');
var stylus = require('stylus');

var _JSON$parse = JSON.parse(process.argv[2]);

var htmlFile = _JSON$parse.htmlFile;
var cssFile = _JSON$parse.cssFile;
var pageConfigFile = _JSON$parse.pageConfigFile;
var outputFile = _JSON$parse.outputFile;
var withGUI = _JSON$parse.withGUI;


var compileStylus = function compileStylus(styleIndex, dimensions) {

    var createStylusPageConfig = function createStylusPageConfig(dimensions) {
        var stylusPageConfig = {};
        Object.keys(dimensions).forEach(function (key) {
            return stylusPageConfig[key] = new stylus.nodes.Literal(dimensions[key]);
        });
        return stylusPageConfig;
    };

    return new Promise(function (resolve, reject) {
        stylus('').define('page', createStylusPageConfig(dimensions), true).set('filename', styleIndex).import(styleIndex).render(function (err, h2pCSS) {

            if (err) {
                reject(err);
            }

            resolve(h2pCSS);
        });
    });
};

var stylesDir = path.resolve(__dirname, '../style');
var config = require(pageConfigFile);

dialog.showErrorBox = function (title, content) {
    throw new Error(title + '\n' + content);
};

function getWindowOptions() {
    if (!withGUI) {
        return { width: 0, height: 0, show: false };
    } else {
        var display = electron.screen.getAllDisplays()[0];
        var _display$workArea = display.workArea;
        var width = _display$workArea.width;
        var height = _display$workArea.height;


        return { width: width, height: height, show: true };
    }
}

app.on('ready', function () {

    var indexHTML = path.resolve(__dirname + '/../window/index.html');
    var window = new BrowserWindow(getWindowOptions());

    window.toggleDevTools();

    window.loadURL('file://' + indexHTML);
    window.webContents.on('did-finish-load', function () {

        compileStylus(stylesDir + '/index.styl', config.page.dimensions).then(function (h2pStyle) {
            global.html = fs.readFileSync(htmlFile, 'utf8');
            global.h2pStyle = h2pStyle;
            global.userStyle = fs.readFileSync(cssFile, 'utf8');
            global.pageConfig = config.page;
            global.postProcess = config.postProcess.toString();
        });
    });

    var print = function print() {
        return window.webContents.printToPDF({}, function (err, data) {
            if (err) {
                throw err;
            }

            fs.writeFile(outputFile, data, function (err) {

                if (err) {

                    throw err;
                }

                if (!withGUI) {
                    app.quit();
                }
            });
        });
    };

    ipcMain.on('done', function (e) {
        console.log('main received done event from renderer');
        print();
    });
});