




module.exports =  {


    page: {
        count: 140,
        mirror: true,

        dimensions: {
            width:      "148mm",
            height:     "210mm",
            cropSize:        "5mm",
            bleed:            "3mm",
            marginInside:    "50mm",
            marginOutside:   "8mm",
            marginBottom:    "25mm",
            marginTop:       "10mm",
            headerHeight:    "10mm",
            footerHeight:    "10mm"
        }
    },


    postProcess: function(pages, $){
        $(pages).find('.page .footer').each((i, footer) => {
            $(footer).text(i);
        });

        $(pages).find('.page').each((i, page) => {


            const $article = $(page).find('.article-content');
            const headerContent = $article.data('header-content');

            $(page).find('.header').text(headerContent);

        });
    }

};