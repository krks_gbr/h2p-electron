
require('babel-register');

const fs = require('fs');
const path = require('path');
const renderPDF = process.env.FROM_SRC ? require('../src/main') : require('../');



let options = {
    htmlFile:       path.resolve(`${__dirname}/files/article.html`),
    cssFile:        path.resolve(`${__dirname}/files/article.css`),
    pageConfigFile: path.resolve(`${__dirname}/page-config.js`),
    outputFile:     path.resolve(`${__dirname}/output/test.pdf`),
    withGUI:        true,
};

renderPDF(options)
    .then(() => console.log("done"))
    .catch(e => console.log(e))
;