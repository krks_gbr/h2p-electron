
const webpack = require('webpack');


let moduleRoot = `${__dirname}/src/window`;


module.exports = {
    target: "electron",
    devtool: "source-map",
    entry: `${moduleRoot}/index.js`,
    output: {
        path: `${__dirname}/dist/window`,
        filename: 'bundle.js',
    },
    module:{
        loaders:[
            {
                test: /\.jsx?$/,
                loaders: ["babel"],
                include: [ moduleRoot ]
            },
            {
                test: /\.html$/,
                loader: "html",
            },
        ]
    },

    plugins: [
        new webpack.optimize.DedupePlugin()
    ]

};